-- CS1501
-- Art Chaidarun (nc5rk)
-- Final project

-- https://bitbucket.org/artnc/cs1501

-- +10pts if it does what you intended
-- +10pts if you opensource the code online (GitHub, etc)
-- +25pts if you document everything

-- Return the prime factorization of n
factor n = factor' n 2
  where
    -- Base case
    factor' 1 _ = []
    -- Recursive case
    factor' n f
      | mod n f < 1 = f : factor' (div n f) f
      | otherwise = factor' n (f + 1)
